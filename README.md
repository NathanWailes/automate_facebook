## How to use this

### Setup
1. Get a list of the usernames of your facebook friends.
    1. Get a text file that has the URLs of your friends' Facebook profiles in it.
        1. Go to the page on Facebook that has your list of friends.
            * Example: https://www.facebook.com/your_username_here/friends
        1. Copy the name of the first friend that shows up.
        1. Keep scrolling down the page until all of your friends have loaded on the page.
        1. Right-click on the page, select 'View page source' (assuming you're using Chrome).
        1. A new tab should pop up with the page source.
        1. Search for the name of the friend whose name you copied earlier.
            * You should be brought to a part of the source that has info about your friends.
        1. Select the section of the source that has info about your friends.
            * Explanation: We're doing this so that we have to do less work later to remove junk
              from our list of usernames.
            * In particular, make sure you see links like this one:
              "https://www.facebook.com/username_of_your_friend"
        1. Right-click and select 'Save as'.
        1. Save the file to 'automate_facebook/setup/facebook friends raw html.txt'.
    1. Run 'get\_facebook\_usernames\_from\_raw\_html.py'.
        * It should output a text file in the 'setup' folder called 'facebook usernames.txt'.
    1. Go through the list of usernames in the outputted 'facebook usernames.txt' and remove any row that
       isn't a username of one of your friends.
        * Example: In my list I had 'MITnews' (which was a page I had liked).
1. Run 'create_database.py' to create the database that will act as the 'long-term memory' of this module.
1. Install the ChromeDriver.
    1. Go to https://sites.google.com/a/chromium.org/chromedriver/downloads
    1. PythonAnywhere is running the 64-bit version of Linux, so download the corresponding .zip file.
    1. Extract the file 'chromedriver'.
    1. In PythonAnywhere, switch to the 'Files' tab.
    1. Navigate to the directory '.virtualenvs/name_of_your_virtualenv_for_this_project/bin'.
    1. Upload the extracted 'chromedriver' file to this directory.
    1. Start a bash session in this directory.
    1. Run the command 'chmod 775 chromedriver' so that the file can be run as an executable.

### Running the program after setup is done

1.


## Misc Ideas
* I think it's good to go through your list of friends and think to yourself, "Do I really want a computer
   automatically posting on this person's timeline?" At least for me, I found that I felt especially nervous about
   posting to the timelines of certain women that I'm friends with.
* I think what I'm going to want to do is:
    1. First create code that'll scrape the first and last name of everyone I'm friends with.
    1. Then create code that'll determine the gender based on the name. (or should I just do it manually?)
    1. Also scrape how many mutual friends we have.
    1. Then, first start by interacting just with men.
    1. After a while, start to slowly interact with women as well.
    1. Maybe rank both the men and women in terms of how often you should be interacting with them.
    1. If your women-friends see you interacting with their man-friends, they may put less meaning into it when you like
    their stuff.
    1. Also, maybe have different thresholds for the number of people who must have already reacted to a post before you
    will also add your reaction. So for a woman you might wait until more people have reacted.

