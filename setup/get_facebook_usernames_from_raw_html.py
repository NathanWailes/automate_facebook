"""
"""

import re


def main():
    """
    :return:
    """
    with open('facebook friends raw html.txt', 'r', encoding='utf-8') as infile, \
         open('facebook usernames.txt', 'w', encoding='utf-8') as outfile:
        data = infile.readlines()
        data = ''.join(data)
        matches = set(re.findall('https://www.facebook.com/([\w\.]+)', data))

        for match in matches:
            if match not in ['profile.php', 'about', 'settings', 'images', 'photo.php']:
                print(match)
                outfile.write(match)


if __name__ == '__main__':
    main()
