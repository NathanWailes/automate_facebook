import sqlite3


def main():
    with sqlite3.connect('automate_facebook/facebook friends.db') as db:
        create_users_table(db)
        create_action_types_table(db)
        create_actions_table(db)


def create_users_table(db):
    # Friends
    db.execute('''create table users
                 (id integer primary key autoincrement,
                  username text)''')

    with open('facebook usernames.txt', 'r') as infile:
        facebook_friend_usernames = [username.strip() for username in infile.readlines()]

    row_values_to_insert = [(None, username) for username in facebook_friend_usernames]
    db.executemany("insert into users values (?, ?)", row_values_to_insert)


def create_action_types_table(db):
    db.execute('''create table action_types
                 (id integer primary key autoincrement,
                  name text)''')
    interaction_types = ['add reaction', 'comment', 'poke', 'post', 'message', 'report', 'block', 'share', 'unfriend',
                         'friend', 'follow', 'view profile']
    row_values_to_insert = [(None, interaction_type) for interaction_type in interaction_types]
    db.executemany("insert into action_types values (?, ?)", row_values_to_insert)


def create_actions_table(db):
    db.execute('''create table actions
                 (id integer primary key autoincrement,
                  actor_id integer,
                  recipient_id integer,
                  action_type_id integer,
                  created_at timestamp,
                  foreign key(recipient_id) references users(id),
                  foreign key(action_type_id) references action_types(id))''')


if __name__ == '__main__':
    main()
