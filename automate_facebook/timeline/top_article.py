import os.path
import sqlite3
import time

from automate_facebook.reactions import get_an_appropriate_reaction_for, add_reaction, \
    get_existing_reactions_given_facebook_article_element
from automate_facebook.timeline import go_to_timeline_of_username
from automate_facebook.utils import PATH_TO_DB, ACTOR_USERNAME
from automate_facebook.utils.db_utils import log_action
from automate_facebook.utils.facebook_utils import log_into_facebook
from automate_facebook.utils.selenium_utils import load_browser


def post_and_log_reaction_to_some_users_top_article():
    """
    :return:
    """
    assert os.path.isfile(PATH_TO_DB)
    with sqlite3.connect(PATH_TO_DB) as db:
        # TODO: Have this grab a username that hasn't had an action done to them in a while.
        recipient_username = db.execute('''select users.username
                                           from users
                                           limit 1''').fetchall()[0][0]

        browser = load_browser(browser_type="Chrome")
        log_into_facebook(browser)

        add_reaction_to_the_top_article_for_username(recipient_username, browser)

        actor_username = ACTOR_USERNAME
        action_to_take = 'add reaction'
        log_action(actor_username, action_to_take, recipient_username, db)


def get_info_about_the_top_article_for_username(username, browser):
    """
    :param username:
    :param browser:
    :return:
    """
    go_to_timeline_of_username(username, browser)
    time.sleep(0.5)
    first_timeline_article = browser.find_element_by_xpath('//div[@aria-label="Story"]')
    info_about_the_article = dict()
    info_about_the_article['existing_reactions'] = get_existing_reactions_given_facebook_article_element(first_timeline_article)
    # Get the date of the article
    # Get the text of the article (if any)
    # The number and type of existing reactions
    # Later: Who it has been shared with?
    # Later: The comments?
    return info_about_the_article


def add_reaction_to_the_top_article_for_username(recipient_username, browser):
    """
    :param recipient_username:
    :param browser:
    :return:
    """
    go_to_timeline_of_username(recipient_username, browser)
    time.sleep(0.5)
    first_timeline_article = browser.find_element_by_xpath('//div[@aria-label="Story"]')
    reaction_type = get_an_appropriate_reaction_for(first_timeline_article)
    add_reaction(reaction_type, first_timeline_article, browser)


if __name__ == '__main__':
    post_and_log_reaction_to_some_users_top_article()
