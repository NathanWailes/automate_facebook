from selenium.webdriver.common.keys import Keys

from automate_facebook.utils.selenium_utils import wait_for_next_page_to_finish_loading


def log_into_facebook(browser):
    browser.get('https://www.facebook.com/')

    email_input_element = browser.find_element_by_id('email')
    email_input_element.send_keys('nathan.wailes@gmail.com')

    password_input_element = browser.find_element_by_id('pass')
    password_input_element.send_keys('l6jhaMBS23kLryD')

    password_input_element.send_keys(Keys.ENTER)
    wait_for_next_page_to_finish_loading(browser)