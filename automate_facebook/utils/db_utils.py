from datetime import datetime


def log_action(actor_username, action_to_take, recipient_name, db):
    actor_id = db.execute('''select id
                             from users
                             where username = ?
                             ''', (actor_username,)).fetchall()[0][0]  # [0][0] selects the first row, first column

    action_type_id = db.execute('''select id
                                   from action_types
                                   where name = ?
                                   ''', (action_to_take,)).fetchall()[0][0]

    user_id_of_friend = db.execute('''select id
                                      from users
                                      where username=?''', (recipient_name,)).fetchall()[0][0]

    db.execute('''insert into actions values (?, ?, ?, ?, ?)''', [None, actor_id, user_id_of_friend, action_type_id,
                                                                  datetime.utcnow()])
