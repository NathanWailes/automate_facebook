import os
from pkg_resources import resource_filename
import requests

PAUSE = 2  # This is designed to be (and is) imported elsewhere.


PATH_TO_DB = resource_filename('automate_facebook', 'facebook friends.db')
ACTOR_USERNAME = 'nathan.wailes'  # TODO: Don't hard-code this.


def download_image(image_url, save_path):
    """
    :param image_url:
    :param save_path:
    :return:
    """
    f = open(save_path, 'wb')
    f.write(requests.get(image_url).content)
    f.close()


def create_dir(directory):
    """
    :param directory:
    :return:
    """
    if not os.path.exists(directory):
        os.makedirs(directory)
