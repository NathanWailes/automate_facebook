import time

from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait

from automate_facebook.utils import PAUSE


def load_browser(browser_type="Firefox"):
    """
    :param browser_type:
    """
    assert browser_type in ["Firefox", "Chrome", "PhantomJS"]
    if browser_type == 'PhantomJS':
        path_to_phantomjs = 'C:/Selenium/phantomjs-2.0.0-windows/bin/phantomjs.exe' # change path as needed
        capabilities = webdriver.DesiredCapabilities.PHANTOMJS.copy()
        capabilities["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (X11; Linux x86_64) "
                                                             "AppleWebKit/53 (KHTML, like Gecko) "
                                                             "Chrome/15.0.87")
        browser = webdriver.PhantomJS(executable_path=path_to_phantomjs, desired_capabilities=capabilities)
    elif browser_type == 'Firefox':  # 2016.08.09 - I can't get this working on my local machine (laptop).
        display = Display(visible=0, size=(800, 600))
        display.start()
        browser = webdriver.Firefox()
    else:  # browser_type == 'Chrome':  # 2016.08.09 - I haven't been able to get Chrome working on PythonAnywhere.
        chrome_options = webdriver.ChromeOptions()
        prefs = {"profile.default_content_setting_values.notifications": 2}  # Disable 'Allow notifications' prompts
        chrome_options.add_experimental_option("prefs", prefs)
        browser = webdriver.Chrome(chrome_options=chrome_options)
    browser.implicitly_wait(3)  # TODO: Figure out the best-practice here.
    return browser


def wait_for_next_page_to_finish_loading(browser):
    """
    :param browser:
    :return:
    """
    timeout = 5
    try:
        element_present = EC.presence_of_element_located((By.ID, 'facebook'))
        WebDriverWait(browser, timeout).until(element_present)
    except TimeoutException:
        print("Timed out waiting for page to load")


def hover_over_element(element, browser):
    """
    :param element:
    :param browser:
    :return:
    """
    hover_action = ActionChains(browser).move_to_element(element)
    hover_action.perform()


def click_element(element, browser):
    """
    :param element:
    :param browser:
    :return:
    """
    ActionChains(browser).move_to_element(element).click().perform()


def scroll_to_bottom_of_page(browser):
    """
    :param browser:
    :return:
    """
    last_height = browser.execute_script("return document.body.scrollHeight")
    while True:
        browser.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(PAUSE)
        new_height = browser.execute_script("return document.body.scrollHeight")
        if new_height == last_height:
            break
        last_height = new_height


def scroll_to_top_of_page(browser):
    """
    :param browser:
    :return:
    """
    browser.execute_script("window.scrollTo(0, 0)")
