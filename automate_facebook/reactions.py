import re
import time

from selenium.common.exceptions import NoSuchElementException

from automate_facebook.utils.selenium_utils import hover_over_element, click_element

REACTION_TYPES = ['Like', 'Love', 'Haha', 'Wow', 'Sad', 'Angry']


def get_number_of_reactions_given_element_and_reaction_type(existing_reactions_element, reaction_type):
    """
    :param existing_reactions_element:
    :param reaction_type:
    :return:
    """
    assert reaction_type in REACTION_TYPES
    xpath_to_find_the_reactions_element = './/a[contains(@aria-label, "{}")]'.format(reaction_type)
    reactions_element = existing_reactions_element.find_element_by_xpath(xpath_to_find_the_reactions_element)
    text_indicating_the_number_of_reactions = reactions_element.get_attribute('aria-label')  # Ex: '1 Like', '8 Like'
    regex_to_capture_the_number_of_reactions = "^(\d+) %s" % reaction_type
    matches = re.findall(regex_to_capture_the_number_of_reactions, text_indicating_the_number_of_reactions)
    if matches:
        number_of_reactions = int(matches[0])
    else:
        number_of_reactions = 0
    return number_of_reactions


def get_an_appropriate_reaction_for(facebook_article_element):
    """ TODO: Implement this function. If other people are posting "Sad" or "Angry", you'll want to do that too.
    :param facebook_article_element:
    :return:
    """
    # pseudocode implementation below:
    # existing_reactions = get_existing_reactions_for_post(facebook_article_element)
    # most_common_reaction = sorted([(reaction_type, count) for reaction_type, count in existing_reactions.items()],
    #                               key=lambda two_tuple: two_tuple[1])
    return 'Like'


def add_reaction(reaction_type, timeline_article, browser):
    """
    :param reaction_type:
    :param timeline_article:
    :param browser:
    :return:
    """
    assert reaction_type in REACTION_TYPES

    button_for_adding_a_reaction = timeline_article.find_element_by_xpath('.//a[contains(@class,"UFILikeLink")]')

    hover_over_element(button_for_adding_a_reaction, browser)
    time.sleep(2)

    reaction_xpath = '//span[@aria-label="{}" and ancestor::div[@aria-label="Reactions"] and ' \
                     'not(ancestor::div[contains(@class,"fb_content")])]'.format(reaction_type)
    reaction_element = button_for_adding_a_reaction.find_element_by_xpath(reaction_xpath)

    click_element(reaction_element, browser)


def get_existing_reactions_given_facebook_article_element(article_element):
    """
    :param article_element: Selenium object
    :return:
    """
    existing_reactions = {reaction_type: 0 for reaction_type in REACTION_TYPES}

    try:
        existing_reactions_element = article_element.find_element_by_xpath('.//span[@aria-label="See who reacted to this"]')
    except NoSuchElementException:
        return existing_reactions

    for reaction_type in REACTION_TYPES:
        try:
            existing_reactions[reaction_type] = get_number_of_reactions_given_element_and_reaction_type(existing_reactions_element, reaction_type)
        except NoSuchElementException:
            pass

    return existing_reactions
